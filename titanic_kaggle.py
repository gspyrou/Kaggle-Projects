
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
from scipy.stats import norm
sns.set(context = 'paper', palette = 'winter_r', style = 'darkgrid', rc= {'figure.facecolor': 'gray',}, font_scale=1.5)



os.chdir("C:\\Users\\george\\Documents\\Kaggle\\Titanic dataset")


# Loading the file
train = pd.read_csv("train.csv",index_col = 'PassengerId')
test = pd.read_csv("test.csv",index_col = 'PassengerId')


'''
Survived: Wheather the person Survived or not.

Pclass: Passanger class indicates the class of that person aboard the ship.

SibSp: Shows the number of Sibling/Spouces they had.

Parch: Parch indicates Parents with children

TIcket: Ticket name/Number.

Fare: How much the Passenger paid.

Cabin: Cabin name of that Passenger.

Embarked: Point of Embarkation where C means Cherbourg, Q means Queenstown, S means Southampton.
'''


# Exploratory Data Analysis

train.head() # Survived, Pclass, Sex, Embarked are categorical
train.isnull().sum() # check how many of them have null values

# Histogram for variable 'Age'
a = sns.distplot(train['Age'].dropna(),bins = range(0,len(set(train['Age'].dropna())),1), rug = False, fit = norm)
plt.title("Histogram of Age Variable")
plt.ylabel("Count")

# We can see that most of the people that died were less than 40 years old.


fig, axs = plt.subplots(ncols=2,figsize=[12,8])
sns.violinplot(x = 'Sex', y = 'Survived', data = train, ax=axs[0])
sns.barplot(x = 'Pclass',y ='Survived',data = train,hue = 'Embarked',ax=axs[1])

# Mosly people who died were males, while the majority of females lived.
# Also the first two categories of Pclass seemed to have people who lived, while people from category 3 died more.
# Also category "S" seems to have the most amount of lethality levels

###